(defproject vise890/zookareg "1.0.1-3"
  :description "Embedded ZOokeeper KAfka and Confluent's Schema REGistry"
  :url "http://gitlab.com/vise890/zookareg"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :repositories {"confluent" "https://packages.confluent.io/maven"}

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [integrant "0.6.3"]
                 [vise890/systema "0.1.1"]

                 [org.clojure/tools.logging "0.4.0"]
                 [org.clojure/tools.namespace "0.2.11"]
                 [me.raynes/fs "1.4.6"]

                 [org.apache.curator/curator-test "4.0.1"]
                 [org.apache.kafka/kafka_2.11 "1.0.1" :exclusions [org.apache.zookeeper/zookeeper]]
                 [io.confluent/kafka-schema-registry "4.0.0" :exclusions [org.apache.kafka/kafka-clients org.apache.kafka/kafka_2.11]]]

  :exclusions [[org.slf4j/slf4j-log4j12]]

  :plugins [[lein-codox "0.10.3"]]

  :resource-paths ["resources"]

  :profiles {:dev  {:source-paths   ["dev/clj"]
                    :resource-paths ["dev/resources"]
                    :dependencies   [[org.slf4j/jcl-over-slf4j "1.7.25"]
                                     [org.slf4j/jul-to-slf4j "1.7.25"]
                                     [org.slf4j/log4j-over-slf4j "1.7.25"]
                                     [org.slf4j/slf4j-api "1.7.25"]
                                     [ch.qos.logback/logback-classic "1.2.3"]
                                     [ch.qos.logback/logback-core "1.2.3"]

                                     [vise890/scribe "0.1.0-SNAPSHOT"]
                                     [kafka-avro-confluent "0.1.0"]]}
             :test {:global-vars {*warn-on-reflection* true}}
             :ci   {:deploy-repositories [["clojars" {:url           "https://clojars.org/repo"
                                                      :username      :env
                                                      :password      :env
                                                      :sign-releases false}]]}})
