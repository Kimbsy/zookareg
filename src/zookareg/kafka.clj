(ns zookareg.kafka
  "Utilities for configuring, starting and halting an embedded Kafka broker."
  (:require [integrant.core :as ig]
            [zookareg.utils :as ut])
  (:import [kafka.server KafkaConfig KafkaServerStartable]))

(defn- ->broker
  "Starts a Kafka broker."
  [config]
  (let [config (KafkaConfig. (ut/m->properties config))
        server (KafkaServerStartable. config)]
    (.startup server)
    server))

(defn shutdown
  "Stops a Kafka broker."
  [^KafkaServerStartable b]
  (when b
    (.shutdown b)
    (.awaitShutdown b)))

(defmethod ig/init-key ::server [_ {:keys [config]}]
  (->broker config))

(defmethod ig/halt-key! ::server [_ b]
  (shutdown b))
