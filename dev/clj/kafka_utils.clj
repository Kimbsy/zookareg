(ns kafka-utils
  (:require [clojure.walk :refer [stringify-keys]]
            [kafka-avro-confluent.deserializers :refer [->avro-deserializer]]
            [kafka-avro-confluent.schema-registry-client :as schema-registry]
            [kafka-avro-confluent.serializers :refer [->avro-serializer]]
            [zookareg.core :as config])
  (:import java.util.UUID
           org.apache.kafka.clients.consumer.KafkaConsumer
           [org.apache.kafka.clients.producer KafkaProducer ProducerRecord]
           [org.apache.kafka.common PartitionInfo TopicPartition]
           [org.apache.kafka.common.serialization StringDeserializer StringSerializer]))

(defn normalize-kafka-config
  "Normalise configuration map into a format required by kafka."
  [config]
  (->> config
       (stringify-keys)
       (map (fn [[k v]] [k (if (number? v) (int v) v)]))
       (into {})))

(defn ->schema-registry
  [config]
  {:post [some?]}
  (schema-registry/->schema-registry-client
   {:base-url (get-in config [:zookareg.schema-registry/server :config :listeners])}))

;;,---------
;;| Producer
;;`---------
(defn with-producer
  [config schema f]
  (let [pc               (normalize-kafka-config (merge (get-in config
                                                                [:zookareg.kafka/server :config])
                                                        {:retries 3}))
        key-serializer   (StringSerializer.)
        schema-registry  (->schema-registry config)
        value-serializer (->avro-serializer schema-registry schema)
        k-producer       (KafkaProducer. pc key-serializer value-serializer)]
    (try
      (f k-producer)
      (finally
        (.close k-producer)))))

(defn produce
  ([config topic schema val]
   (with-producer config schema
     (fn [p]
       (deref (.send p (ProducerRecord. topic
                                        (str (UUID/randomUUID))
                                        val)))
       (.flush p))))
  ([topic schema val]
   (produce (config/read-default-config) topic schema val)))

;;,---------
;;| Consumer
;;`---------
(defn- PartitionInfo->TopicPartition [^PartitionInfo pi]
  (TopicPartition. (.topic pi) (.partition pi)))
(defn- subscribed-PartitionInfos [^KafkaConsumer k-consumer]
  (->> k-consumer
       .subscription
       (mapcat #(.partitionsFor k-consumer %))))

(defn- subscribed-TopicPartitions [^KafkaConsumer k-consumer]
  (->> k-consumer
       subscribed-PartitionInfos
       (map PartitionInfo->TopicPartition)))

(defn assign-partitions [^KafkaConsumer consumer topic-partitions]
  (->> topic-partitions
       (map #(TopicPartition. (:topic %) (:partition %)))
       (.assign consumer)))
(defn assign-partition-0 [^KafkaConsumer consumer topic]
  (.assign consumer [(TopicPartition. topic 0)]))

(defn subscribe [consumer & topics] (.subscribe consumer topics))
(defn unsubscribe [consumer] (.unsubscribe consumer))

(defn seek-to-end [consumer]
  (.poll consumer 0)
  (.seekToEnd consumer [])
  ;; NOTE we poll to force the seek, assuming it's ok to discard any messages on
  ;; the topic when this is called
  (.poll consumer 0))

(defn seek-to-beginning [consumer]
  (.poll consumer 0)
  ;; NOTE we don't poll here, as we don't wanna discard the messages
  (.seekToBeginning consumer []))

(defn with-consumer
  ([config f]
   (let [cc                 (normalize-kafka-config
                             (merge (get-in config [:zookareg.kafka/server :config])
                                    {:group.id (str "tests-"
                                                    (UUID/randomUUID))}))
         key-deserializer   (StringDeserializer.)
         schema-registry    (->schema-registry config)
         value-deserializer (->avro-deserializer schema-registry)
         k-consumer         (KafkaConsumer. cc
                                            key-deserializer
                                            value-deserializer)]
     (try
       (f k-consumer)
       (finally
         (.close k-consumer)))))
  ([f]
   (with-consumer (config/read-default-config) f)))

(defn- ConsumerRecord->m [cr]
  (-> cr
      .value
      (with-meta (merge {:offset    (.offset cr)
                         :partition (.partition cr)
                         :topic     (.topic cr)
                         :timestamp (.timestamp cr)
                         :key       (.key cr)}
                        (meta (.value cr))))))

(defn committed-info [consumer]
  (->> (subscribed-TopicPartitions consumer)
       (map (fn [tp]
              {:topic     (.topic tp)
               :partition (.partition tp)
               :offset    (.offset (.committed consumer tp))}))))

(defn poll*
  [consumer & {:keys [expected-msgs retries poll-timeout]
               :or   {expected-msgs 1
                      retries       500
                      poll-timeout  20}}]
  (loop [received []
         retries  retries]
    (if (or (>= (count received) expected-msgs)
            (zero? retries))
      received
      (recur (concat received
                     (map ConsumerRecord->m
                          (.poll consumer poll-timeout)))
             (dec retries)))))

(defn with-1-partition-consumer-from-end
  ([config topic f]
   (with-consumer
     (fn [kc]
       (assign-partition-0 kc topic)
       (seek-to-end kc)
        ;; NOTE seek is lazy, so we force a poll here
        ;; **YOU SHOULD CALL THIS BEFORE MESSAGES ARE PRODUCED, OR THIS POLL
        ;; WILL CONSUME THEM**
       (.poll kc 1)
       (f kc))))
  ([topic f]
   (with-1-partition-consumer-from-end (config/read-default-config) topic f)))
